#include <stdio.h>
#include <stdlib.h>

/*
* Author: Mauricio Del Río
* Version: 0.2
* Description: Start with an integer n. If n is even, divide by 2. If n is odd,
  multiply by 3 and add 1. Repeat this process with the new value of n,
  terminate when n = 1.

  For an input n, the cycle-length of n is the number of numbers generated up to and
  including the 1. In the example above, the cycle length of 22 is 16. Given any two
  numbers i and j, you are to determine the maximum cycle length over all numbers
  between i and j, including both endpoints.
* -------------------------
* Implements: Simple List.
* -------------------------
* Executing: ./exec n1 n2   -> where n1 is a inferior limit to evaluate, n2 is a superior
  limit to evaluate.
*-------------------------
* Return:
*--------------------------
* n1 n2 maxCicle   -> where maxCicle is a biggest list.
*
*/

/* struct List for append a result */
typedef struct List{
  int data;
  struct List *next;
}list_t;

/* declare functions */
list_t * initList(list_t *L);
list_t * appendList(list_t *L, int element);
int countList(list_t *L);
void printList(list_t *L);
void freeList(list_t *L);
int function_3n_1(int number);

/* declare & run main code */
int main(int argc, char const *argv[]) {
  int number1 = atoi(argv[1]);
  int number2 = atoi(argv[2]);
  int diff = number2 - number1;
  list_t *L;
  int max = -1;
  int count_actual = 0;
  int num_eval = number1;
  int num_actual = number1;
  for (int i = 0; i < diff; i++) {
    num_eval = num_actual;
    L = initList(L);
    L = appendList(L, num_eval);
    while (num_eval != 1) {
      num_eval = function_3n_1(num_eval);
      L = appendList(L, num_eval);
    }
    count_actual = countList(L);
    if (max < count_actual) {
      max = count_actual;
    }
    freeList(L);
    num_actual ++;
  }

  //printList(L);
  printf("%d %d %d\n",number1, number2, max);
  return 0;
}

int function_3n_1(int number){
  if (number == 1) {
    return number;
  }

  if (number % 2 == 0) {
    number = number/2;
    return number;
  }

  else{
    number = 3*number + 1;
    return number;
  }
}

list_t * initList (list_t *L){
  L = NULL;
  return L;
}

list_t * appendList(list_t *L, int element){
  list_t *p, *q;

	p = calloc(1,sizeof(list_t));
	p->data = element;
	p -> next = NULL;
	if (L==NULL)
	{
		/* code */
		return p;
	}

	if (L->next == NULL)
	{
		/* code */
		L->next = p;
		return L;
	}

	q = L->next;

	while (q-> next != NULL){

		q = q->next;
	}

	q->next = p;
	return L;
}

void printList(list_t *L){
  while (L!=NULL){

    printf("%d\n", L->data );
    L = L->next;
  }
}

int countList (list_t *L){
  int contador = 0;
  while (L!=NULL) {
    contador ++;
    L = L->next;
  }
  return contador;
}

void freeList(list_t *L){
   list_t* tmp;

   while (L != NULL)
    {
       tmp = L;
       L = L->next;
       free(tmp);
    }

}
